import React, { Component } from 'react';
import { styles } from './styles/app.scss';

export default class App extends Component {
  render() {
    return (
      <div className={`${styles}`}>
          React simple starter 4
      </div>
    );
  }
}
