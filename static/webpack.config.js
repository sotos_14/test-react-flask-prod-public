// var combineLoaders = require('webpack-combine-loaders');
// var ExtractTextPlugin = require('extract-text-webpack-plugin');
// 
// const path = require('path');
// 
// const PATHS = {
//     app: path.join(__dirname, './src'),
//     build: path.join(__dirname, './dist'),
// };
// 
// module.exports = {
//     entry: [
//         PATHS.app,
//     ],
//     output: {
//         path: PATHS.build,
//         filename: 'bundle.js',
//     },
//     module: {
//         loaders: [{
//             exclude: /node_modules/,
//             loader: 'babel',
//             query: {
//                 presets: ['react', 'es2015', 'stage-1']
//             }
//         },
//         {
//             test: /\.css$/,
//             loader: ExtractTextPlugin.extract(
//                 'style-loader',
//                 combineLoaders([{
//                     loader: 'css-loader',
//                     query: {
//                         modules: true,
//                         localIdentName: '[name]__[local]___[hash:base64:5]'
//                     }
//                 }])
//             )
//         }]
//     },
//     resolve: {
//         extensions: ['', '.js', '.jsx', '.json', '.css'],
//         modulesDirectories: ['node_modules', PATHS.app],
//     },
//     devServer: {
//         historyApiFallback: true,
//         contentBase: './'
//     },
//     plugins: [
//         new ExtractTextPlugin('styles-[hash].css'),
//     ],
// };
